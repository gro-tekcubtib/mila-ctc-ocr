# AI4good hackathon
### Challenge: OCR from tiny images of doctor's handwriting
###### Date: June 7-8-9th 2019
###### Location: Montreal, QC, Canada

### Project description
<b>Charitable Analytics International </b> (CAI) is a Montreal-based NGO that is working to improve the digitization of health records.

They partner with organizations that operate in over 80 countries, where they deliver much-needed supplies to health clinics, schools, refugee camps, etc.

The areas where they operate from often lack computers; so paper is the only form of bookkeeping possible.

The United Nations (UN) have partnered with CAI to automatically digitize pictures of these log books sent to them via Whatsapp.

The goal is to process tiny images (see below) and extract the information that is handwritten in them.

### Results
In just a little under 48 hours, we were able to produce a solution that would correctly identify what is written in <b>85%</b> of theses tiny images.

### Solution
The solution implements a CNN-LSTM-CTC architecture (see other implementations [here](https://github.com/weinman/cnn_lstm_ctc_ocr) or [here](https://github.com/watsonyanghx/CNN_LSTM_CTC_Tensorflow) and paper [here](https://www.cs.toronto.edu/~graves/icml_2006.pdf))
<img src="https://bitbucket.org/gro-tekcubtib/mila-ctc-ocr/raw/a0550efbc11dcbc5717f8f3d5d95a86a78957645/intuitionInANutshell.jpg" width="60%"></img>

### How to use the code
##### 1. Set up your environment
  For this, you can use `pipenv` (see doc [here](https://github.com/pypa/pipenv)) to handle all of your python package dependencies.

Make sure to install `virtualenv` and `pipenv`
  
  > python -m pip install virtualenv
  
  > python -m pip install pipenv

Then, install the virtual environment according to the specs in `Pipfile` and `Pipfile.lock`

> cd /path/to/this/repo

> pipenv install

Finally, use the subshell, provided by `pipenv` that contains a python virtualenv with all of the adequate dependencies installed.

> pipenv shell

**NOTE:** The following commands are ran from the shell started by the command `pipenv shell`, and thus assumes that you have all of the relevant packages along your python version.

##### 2. The data
The data is located in `./code/imgs/train` and `./code/imgs/infer`.

The code expects a special naming convention of the images in `./code/imgs/train`. These will have to be named like so: `\<somename>_\<label>.jpg`

<u>Example</u>

`Label`: -3,000.25

`Valid name`: greatpicture_-3,000.25.jpg or pic1_-3,000.25.jpg or etc_-3000.25.jpg

Thankfully, you can use the script in `./code/misc/processImg.py` to do the naming convention for you.

##### 3. Training
To perform the training, you can call `main.py` from your terminal.

> CUDA_VISIBLE_DEVICES=0 python ./main.py --train_dir=./imgs/train/   --val_dir=./imgs/val/
   --image_height=40   --image_width=195   --image_channel=1   --out_channels=64
   --num_hidden=128   --batch_size=128   --log_dir=./log/train   --num_gpus=1
   --mode=train --initial_learning_rate 0.001 --restore True
   --save-steps 150

**What do these parameters do?**

**--image_height=40**: Will resize your image to a height of 40 pixels. (it keeps the aspect ratio)

**--image_width=195**: After resizing your image's height to 40, this parameters will pad your image with white pixels of the left and on the right. It puts 15 white pixels to the left. And the balance of the needed pixels to the right.

**--out_channels=64**: Number of channels in the CNN part

**--num_hidden=128**: Size of the LSTM state

**--restore True**: True/False. Restore from `./checkpoint` if True

**--save-steps 150**: Save model to `./checkpoint` each 150 batches

**NOTE**: During training, we trained with 50 epochs at `initial_learning_rate=0.001` and 25 epochs with `initial_learning_rate=0.0005`

##### 4. Inference
To perform the inference, you can call `main.py` in your terminal.

> CUDA_VISIBLE_DEVICES=0 python ./main.py --infer_dir=./imgs/infer/
 --checkpoint_dir=./checkpoint/   --num_gpus=0   --mode=infer --batch_size 1
 --max_stepsize 8 --image_height=40 --image_width=195

 This will save the results here: `./result.txt`
