'''
Author: Alex-Antoine Fortin
Date: Saturday, June 8th 2019
Description
[] - load images
[] - grayscale
[] - deblur
[] - save with labelname
'''

# Import the modules
import pandas as pd
import numpy as np
import cv2
import os
import random

inputPath = '../../AI4Good---Meza-OCR-Challenge/cell_images/training_set/'
outputPath = './imgs/'

labelsdf = pd.read_csv('../inputs/training_set_values.txt', header=0, delimiter=';') # Read label names from textfile

#=====================
# Function to load img
#=====================
def clip(buf):
    grayscale = np.mean(buf, axis=2)
    #print(grayscale.shape)
    flat = np.sort(grayscale.reshape(-1))
    top = flat[int(flat.shape[0] * 0.3)]
    bottom = flat[int(flat.shape[0] * 0.05)]
    buf = (buf - bottom) / (top - bottom)
    buf = np.clip(buf, 0, 1)
    buf = (buf*255).astype('uint8')
    return buf


fileList = [f for f in os.listdir(inputPath) if (os.path.isfile(os.path.join(inputPath, f)) and '._' not in f)]

for files in fileList:
    '''process images and save to disk with new label name'''
    img_path = os.path.join(os.path.join(inputPath, files)) 
    im = cv2.imread(img_path) # Load images
    im = clip(im) # The array representing the image now has values between [0, 1] instead of [0, 255]
    # CV2 operations..
    im_gray = cv2.GaussianBlur(im, (3, 3), 0) # Denoising
    im_gray = cv2.cvtColor(im_gray, cv2.COLOR_BGR2GRAY) # Converting to grayscale
    im_gray = cv2.convertScaleAbs(im_gray) # Very cryptic, unidentified code line. dunno it's purpose. Use with care. Perhaps discard
    # ret, im_th = cv2.threshold(im_gray, 125, 255, cv2.THRESH_BINARY_INV) # Convert image to black & white instead of grayscale
    label = labelsdf[labelsdf.filename==files].value.values[0] # Get label from DataFrame of labels
    # save train/test split. During the hackathon, we trained on 100% of the labeled images
    if random.random()<0.1:
        os.makedirs(os.path.join(outputPath, 'val'), exist_ok=True)
        cv2.imwrite(os.path.join(outputPath, 'val', files.split('.jpg')[0]+'_'+label+'.jpg'), im_gray)
    else:
        os.makedirs(os.path.join(outputPath, 'train'), exist_ok=True)
        cv2.imwrite(os.path.join(outputPath, 'train', files.split('.jpg')[0]+'_'+label+'.jpg'), im_gray)
