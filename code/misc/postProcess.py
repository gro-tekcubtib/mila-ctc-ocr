'''
Author: Alex-Antoine Fortin
Date: Sunday June 9th 2018
Description
Post processing script that sanitize and report on predicted results
[ ] - ..
[ ] - ,,
[?] - ., --> ,
[ ] - - not at the right place
'''

import pandas as pd
df = pd.read_csv('./result.txt', header=0, delimiter=';')

df['value'] = df['value'].str.replace(',,', ',')
df['value'] = df['value'].str.replace('..', '.')
df['value'] = df['value'].str.replace(',.', ',')
df['value'] = df['value'].str.replace('.,', '.')

print(df.sample(10))
#df.to_csv('./result-processed.txt', index=False, sep=';')
