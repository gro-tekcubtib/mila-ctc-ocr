'''
Author: Alex-Antoine Fortin
Date: Saturday, June 8th 2019
Description
[] - load images
[] - grayscale
[] - deblur
[] - save with labelname
'''

# Import the modules
import numpy as np
import cv2
import os
import random

inputPath = '../../AI4Good---Meza-OCR-Challenge/cell_images/validation_set/'
outputPath = './imgs/infer'

#=====================
# Function to load img
#=====================
def clip(buf):
    grayscale = np.mean(buf, axis=2)
    #print(grayscale.shape)
    flat = np.sort(grayscale.reshape(-1))
    top = flat[int(flat.shape[0] * 0.3)]
    bottom = flat[int(flat.shape[0] * 0.05)]
    buf = (buf - bottom) / (top - bottom)
    buf = np.clip(buf, 0, 1)
    buf = (buf*255).astype('uint8')
    return buf


fileList = [f for f in os.listdir(inputPath) if (os.path.isfile(os.path.join(inputPath, f)) and '._' not in f)]

for files in fileList:
    '''process images and save to disk with new label name'''
    img_path = os.path.join(os.path.join(inputPath, files))
    im = cv2.imread(img_path)
    im = clip(im)
    # CV2 operations..
    im_gray = cv2.GaussianBlur(im, (3, 3), 0)
    im_gray = cv2.cvtColor(im_gray, cv2.COLOR_BGR2GRAY)
    im_gray = cv2.convertScaleAbs(im_gray)
    # Thresholding (black-and-white-more)
    ret, im_th = cv2.threshold(im_gray, 125, 255, cv2.THRESH_BINARY_INV)
    # save
    os.makedirs(outputPath, exist_ok=True)
    cv2.imwrite(os.path.join(outputPath, files), im_gray)
